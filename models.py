from sqlalchemy import sql, orm
from app import db
from sqlalchemy import text

class Countries(db.Model):
    __tablename__ = 'countries'
    name = db.Column('name', db.String(20))
    ioc_code = db.Column('ioc_code', db.String(20), primary_key=True)

class Games(db.Model):
    __tablename__ = 'games'
    host_name = db.Column('host_name', db.String(20))
    year = db.Column('year', db.Integer(), primary_key=True)

class Events(db.Model):
    __tablename__ = 'events'
    year = db.Column('year', db.String(20),
                    db.ForeignKey('games.year'), primary_key=True)
    sport = db.Column('sport', db.String(20), primary_key=True)
    discipline = db.Column('discipline', db.String(20), primary_key=True)
    event = db.Column('event', db.String(20), primary_key=True)
    gender = db.Column('gender', db.String(20), primary_key=True)

class Athletes(db.Model):
    __tablename__ = 'athletes'
    name = db.Column('name', db.String(20), primary_key=True)
    gender = db.Column('gender', db.String(20))
    vote = db.Column('vote', db.Integer())
    @staticmethod
    def castVote(athleteName, athleteGender, oldVote):
        voteDB = db.engine.execute(text("SELECT vote FROM Athletes WHERE name=" + "'" + athleteName + "'"))
        vote = 1
        for v in voteDB:
            vote += v.vote
        db.engine.execute(text("UPDATE Athletes SET vote = " + str(vote) +" WHERE name = "+ "'" + athleteName + "'"))

class MedalIn(db.Model):
    __tablename__ = 'medalin'
    year = db.Column('year', db.String(20),
                    db.ForeignKey('games.year'), primary_key=True)
    sport = db.Column('sport', db.String(20), primary_key=True)
    discipline = db.Column('discipline', db.String(20), primary_key=True)
    event = db.Column('event', db.String(20), primary_key=True)
    gender = db.Column('gender', db.String(20))
    medal = db.Column('medal', db.String(20), primary_key=True)
    name = db.Column('name', db.String(20), primary_key=True)
    ioc_code = db.Column('ioc_code', db.String(20))