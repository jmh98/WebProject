import csv
import json

def writeWorldMapJson():
	countries = db.session.query(models.Countries).all()
	sql = text('SELECT Countries.ioc_code, count, Countries.name FROM Countries, (SELECT ioc_code, COUNT(*) FROM MedalIn GROUP BY ioc_code) AS Counts WHERE Countries.ioc_code = Counts.ioc_code')
	result = db.engine.execute(sql)
	results = []
	for row in result:
		results.append(row)

	filename = '/static/js/incomplete_countries.json'
	with open(filename, 'r') as f:
	    data = json.load(f)
	    countries = data["features"]

	    for country in countries:

	    	country["properties"]["count"] = 0
	    	for row in results:
	    		if str(country["id"]).strip() == str(row[0]).strip():
	    			country["properties"]["count"] = int(row[1])
	    			break
	    		
	os.remove(filename)
	with open(filename, 'w') as f:
	    json.dump(data, f, indent=4)

def writeAthleteCsv():
	athletes = db.session.query(models.Athletes).all()
	athleteBubbles = db.engine.execute(text('SELECT name, COUNT(*) FROM MedalIn GROUP BY name ORDER BY count DESC LIMIT 75'))
	outfile = open('static/data/allathletes.csv', 'wb')
	outcsv = csv.writer(outfile)
	outcsv.writerow(["name","count"])
	outcsv.writerows(athleteBubbles)
	outfile.close()