from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField
from wtforms.validators import DataRequired
from sqlalchemy import text

class FormFactory: 
    @staticmethod
    def countryComparisonForm(countries, medals):
        class F(FlaskForm):
            countryA = SelectField(u"CountryA", choices=[(country.name, country.name) for country in countries])
            countryB = SelectField(u"CountryB", choices=[(country.name, country.name) for country in countries])
            submit = SubmitField("Submit")
        return F()