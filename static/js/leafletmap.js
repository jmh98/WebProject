var map = L.map( 'map', {
    center: [30.0, 0.0],
    minZoom: 2,
    zoom: 2
});

var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	opacity: 0.0
}).addTo(map);

//code to combine results of database query with geoJSON data into single json file

// d3.csv("/static/data/test.csv", function(data) {
//   data.forEach(function(d) {
//     d.id = d.id;
//     d.count = +d.count;
//   });
//   $.each(countriesData.features, function(i, shape){
//   	shape.properties.count = 10000000;
//   	$.each(data, function(j, medalCount){
//   		if(String(data[j].id) == String(shape.id)){
//   			shape.properties.count = medalCount.count;
//   		}
//   	});
//   });
// });

// JSON.stringify(countriesData);


// control that shows state info on hover
var info = L.control();
info.onAdd = function (map) {
	this._div = L.DomUtil.create('div', 'info');
	this.update();
	return this._div;
};
info.update = function (props) {
	this._div.innerHTML = '<h4>Total Medals by Country</h4>' +  (props ?
		'<b>' + props.name + '</b><br />' + props.count
		: 'Hover over a country');
};
info.addTo(map);

// get color depending on budget value
function getColor(d) {
	return d > 1000 ? '#08306b' :
		   d > 500 ? '#084594' :
	       d > 200  ? '#2171b5' :
	       d > 100  ? '#4292c6' :
	       d > 50  ? '#6baed6' :
	       d > 20   ? '#9ecae1' :
	       d > 10   ? '#c6dbef' :
	       d > 0  ?   '#deebf7':
	       '#d3d3d3';
}

function style(feature) {
	return {
		weight: 1,
		opacity: 1,
		color: 'white',
		dashArray: '',
		fillOpacity: 0.7,
		fillColor: getColor(feature.properties.count)
	}
}
var geojson;

function highlightFeature(e) {
	var layer = e.target;
	layer.setStyle({
		weight: 1,
		color: '#666',
		dashArray: '',
		fillOpacity: 0.7
	});
	if (!L.Browser.ie && !L.Browser.opera) {
		layer.bringToFront();
	}
	info.update(layer.feature.properties);
}


function resetHighlight(e) {
	geojson.resetStyle(e.target);
	info.update();
}

function zoomToFeature(e) {
	map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
	layer.on({
		mouseover: highlightFeature,
		mouseout: resetHighlight,
		//click: zoomToFeature
	});
	layer.bindPopup(feature.properties.name);
	
}



$.getJSON("static/js/incomplete_countries.json", function(data){
	geojson = L.geoJson(data, {
		style: style,
		onEachFeature: onEachFeature
	}).addTo(map);
});

var legend = L.control({position: 'bottomright'});
legend.onAdd = function (map) {
	var div = L.DomUtil.create('div', 'info legend'),
		grades = [0, 10, 20, 50, 100, 200, 500, 1000],
		labels = [],
		from, to;
	for (var i = 0; i < grades.length; i++) {
		from = grades[i];
		to = grades[i + 1];
		labels.push(
			'<i style="background:' + getColor(from + 1) + '"></i> ' +
			from + (to ? '&ndash;' + to : '+'));
	}
	div.innerHTML = labels.join('<br>');
	return div;
};
legend.addTo(map);