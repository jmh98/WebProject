var svg = d3.select("#graph1").select("svg"),
          width = +svg.attr("width");

var format = d3.format(",d");

// var color = d3.scaleOrdinal(d3.schemeCategory20c);

var pack = d3.pack()
    .size([width, width])
    .padding(1);

d3.csv("static/data/allathletes.csv", 
  function(d) {
    d.count = +d.count;
    if (d.count) return d;
    },

  function(error, countries) {
    if (error) throw error;

    var root = d3.hierarchy({children: countries})
      .sum(function(d) { return d.count; })
      .each(function(d) {
        d.name=d.data.name;
        d.count=d.data.count;
        // if (id = d.data.name) {
        //   var id, i = id.lastIndexOf(".");
        //   d.name = id;
        //   d.count=d.data.count
        //   d.package = id.slice(0, i);
        //   d.class = id.slice(i + 1);
        // }
      });

    var node = svg.selectAll(".node")
      .data(pack(root).leaves())
      .enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; 
      });

    

    node.append("circle")
        .attr("id", function(d) { return d.name; })
        .attr("r", function(d) { return d.r; })
        .style("fill", 
          function(d) {
            var normalized = d3.scaleLinear()
              .domain([0, 30])
              .range([0,1]); 
            return d3.interpolateBlues(normalized(d.count));  
          });

    node.append("clipPath")
        .attr("id", function(d) { return "clip-" + d.name; })
      .append("use")
        .attr("xlink:href", function(d) { return "#" + d.name; });

    node.append("text")
        .attr("clip-path", function(d) { return "url(#clip-" + d.name + ")"; })
        .style("font-size", function(d) { return Math.min(0.18 * d.r, (0.18 * d.r - 0.0008) / this.getComputedTextLength() * 10) + "px"; })
      .selectAll("tspan")
      .data(function(d) { return d.name; })
      
      .enter().append("tspan")
        .attr("x", 0)
        .attr("y", function(d, i, nodes) { return 13 + (i - nodes.length / 2 - 0.5) * 10; })
        
        .attr("x", function(d){ return d.x; })
        .attr("y", function(d){ return d.y ; })
        .text(function(d) { return d; })
        .attr("text-anchor", "middle")
        .style("fill","white")
        .style("font-family","Helvetica Neue, Helvetica, Arial, san-serif")
   
        // .style("font-size", function(d) { 
        //   var r = d3.select(this.parentNode).attr("r");
        //   return Math.min(2 * r, (2 * r - 8) / this.getComputedTextLength() * 24) + "px"; })
        ;

    node.append("title")
        .text(function(d) { return d.name + "\n" + format(d.count); });
  });

