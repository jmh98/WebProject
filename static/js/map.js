 var svg = d3.select("#graph2").select("svg"),
            width = +svg.attr("width")


var projection = d3.geo.mercator();

var path = d3.geo.path()
  .projection(projection);
var g = svg.append("g");

d3.json("world-110m2.json", function(error, topology) {
  g.selectAll("path")
    .data(topojson.object(topology, topology.objects.countries)
        .geometries)
  .enter()
    .append("path")
    .attr("d", path)
});