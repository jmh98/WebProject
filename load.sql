\COPY Countries(name, ioc_code) FROM 'data/country.csv' WITH DELIMITER ',' CSV
\COPY Games(host_name, year) FROM 'data/games.csv' WITH DELIMITER ',' CSV
\COPY Events(year, sport, discipline, event, gender) FROM 'data/events.csv' WITH DELIMITER ',' CSV
\COPY Athletes(name, gender, vote) FROM 'data/athletes.csv' WITH DELIMITER ',' CSV
\COPY MedalIn(year, sport, discipline, event, gender, medal, name, ioc_code) FROM 'data/medalin.csv' WITH DELIMITER ',' CSV
