1. To create the PostgreSQL database and load it with data from raw .csv files, run “./setup.sh” from the root WebProject folder.
2. In config.py, change "osboxes" in the first line to host username.
3. Run “python app.py” also from the root WebProject folder to start up a local copy of the webserver, configured to operate on port 5000.
4. Open a web browser and go to the URL “localhost:5000” to open the homepage of our web site.