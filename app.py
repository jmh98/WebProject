from flask import Flask, render_template, redirect, url_for, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text
import models
import forms
import csv
import json
import os

app = Flask(__name__)
app.secret_key = 's3cr3t'
app.config.from_object('config')
db = SQLAlchemy(app, session_options={'autocommit': False})

@app.route('/')
def home():
	return render_template('home.html')

@app.route('/compare_countries', methods=['GET', 'POST'])
def compare_countries():
	countries = db.session.query(models.Countries).all()
	medals = db.session.query(models.MedalIn).all()
	form = forms.FormFactory.countryComparisonForm(countries, medals)
	if form.is_submitted():
		countryA = form.countryA.data
		countryB = form.countryB.data
		iocA = db.engine.execute(text("SELECT ioc_code FROM Countries WHERE name = " + "'" + countryA + "'")).fetchall()[0].ioc_code.strip()
		iocB = db.engine.execute(text("SELECT ioc_code FROM Countries WHERE name = " + "'" + countryB + "'")).fetchall()[0].ioc_code.strip()
		goldA = db.engine.execute(text("SELECT COUNT(*) FROM MedalIn WHERE medal='Gold' GROUP BY ioc_code HAVING ioc_code= " + "'" + iocA + "'"))
		goldB = db.engine.execute(text("SELECT COUNT(*) FROM MedalIn WHERE medal='Gold' GROUP BY ioc_code HAVING ioc_code= " + "'" + iocB + "'"))
		silverA = db.engine.execute(text("SELECT COUNT(*) FROM MedalIn WHERE medal='Silver' GROUP BY ioc_code HAVING ioc_code= " + "'" + iocA + "'"))
		silverB = db.engine.execute(text("SELECT COUNT(*) FROM MedalIn WHERE medal='Silver' GROUP BY ioc_code HAVING ioc_code= " + "'" + iocB + "'"))
		bronzeA = db.engine.execute(text("SELECT COUNT(*) FROM MedalIn WHERE medal='Bronze' GROUP BY ioc_code HAVING ioc_code= " + "'" + iocA + "'"))
		bronzeB = db.engine.execute(text("SELECT COUNT(*) FROM MedalIn WHERE medal='Bronze' GROUP BY ioc_code HAVING ioc_code= " + "'" + iocB + "'"))
		mostDecoratedA = db.engine.execute(text("SELECT MedalIn.name, COUNT(*) FROM Countries, MedalIn WHERE Countries.ioc_code = MedalIn.ioc_code AND Countries.name = " + "'" + countryA + "'" + "GROUP BY MedalIn.name ORDER BY count DESC LIMIT 1"))
		mostDecoratedB = db.engine.execute(text("SELECT MedalIn.name, COUNT(*) FROM Countries, MedalIn WHERE Countries.ioc_code = MedalIn.ioc_code AND Countries.name = " + "'" + countryB + "'" + "GROUP BY MedalIn.name ORDER BY count DESC LIMIT 1"))
		mostMedalsPerGameA = db.engine.execute(text("SELECT Games.year, host_name, count FROM Games, (SELECT DISTINCT year, COUNT(*) FROM MedalIn GROUP BY year, ioc_code HAVING ioc_code = " + "'" + iocA + "'" + " ORDER BY COUNT(*) DESC LIMIT 1) As Most WHERE Games.year = Most.year"))
		mostMedalsPerGameB = db.engine.execute(text("SELECT Games.year, host_name, count FROM Games, (SELECT DISTINCT year, COUNT(*), ioc_code FROM MedalIn GROUP BY year, ioc_code HAVING ioc_code = " + "'" + iocB + "'" + " ORDER BY COUNT(*) DESC LIMIT 1) As Most WHERE Games.year = Most.year"))
		return render_template('country.html', form=form, countryA=countryA, countryB=countryB, goldA=goldA, goldB=goldB, silverA=silverA, silverB=silverB, bronzeA=bronzeA, bronzeB=bronzeB, mostDecoratedA=mostDecoratedA, mostDecoratedB=mostDecoratedB, mostMedalsPerGameA=mostMedalsPerGameA, mostMedalsPerGameB=mostMedalsPerGameB)
	else:
		return render_template('country.html', form=form)

@app.route('/votes', methods=['GET', 'POST'])
def votes():
	countries = db.session.query(models.Countries).all()
 	if request.method == 'POST':
 		country = request.form.get('country')
  		athletes = db.engine.execute(text("SELECT DISTINCT MedalIn.name FROM MedalIn, Countries WHERE MedalIn.ioc_code = Countries.ioc_code AND Countries.name = " + "'" + country + "'" + "ORDER BY name")).fetchall()
  		return render_template("votes.html", countries=countries, athletes=athletes, initial=False)
 	else:
  		return render_template("votes.html", countries=countries, initial=True)


@app.route('/vote_results', methods=['POST', 'GET'])
def vote_results():
	athlete = request.form.get('athlete')
	userVote=None
	if athlete is not None:
		athleteRow = db.engine.execute(text("SELECT * FROM Athletes WHERE name = " + "'" + athlete + "'")).fetchall()
		for row in athleteRow:
			models.Athletes.castVote(row.name, row.gender, row.vote)
		userVote = db.engine.execute(text("SELECT name, vote FROM Athletes WHERE name = " + "'" + athlete + "'")).fetchall()
	topTen = db.engine.execute(text("SELECT DISTINCT Athletes.name , vote, ioc_code, discipline  FROM Athletes,MedalIn  WHERE Athletes.name=MedalIn.name  ORDER BY vote DESC LIMIT 10"))
	return render_template('vote_results.html', topTen=topTen, userVote=userVote)

@app.route('/olympians')
def top_athletes():
	return render_template('olympians.html')
	
if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000)
