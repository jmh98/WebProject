import csv

def transform():
	countryDict = {'Kosovo':'KOS','Afghanistan':'AFG', 'Albania':'ALB', 'Algeria':'ALG', 'American Samoa':'ASA', 'Andorra':'AND', 'Angola':'ANG', 'Antigua and Barbuda':'ANT', 'Argentina':'ARG', 'Armenia':'ARM', 'Aruba':'ARU', 'Australia':'AUS', 'Austria':'AUT', 'Azerbaijan':'AZE', 'Bahamas':'BAH', 'Bahrain':'BRN', 'Bangladesh':'BAN', 'Barbados':'BAR', 'Belarus':'BLR', 'Belgium':'BEL', 'Belize':'BIZ', 'Bermuda':'BER', 'Benin':'BEN', 'Bhutan':'BHU', 'Bolivia':'BOL', 'Bosnia and Herzegovina':'BIH', 'Botswana':'BOT', 'Brazil':'BRA', 'British Virgin Islands':'IVB', 'Brunei':'BRU', 'Bulgaria':'BUL', 'Burkina Faso':'BUR', 'Burundi':'BDI', 'Cambodia':'CAM', 'Cameroon':'CMR', 'Canada':'CAN', 'Cape Verde':'CPV', 'Cayman Islands':'CAY', 'Central African Republic':'CAF', 'Chad':'CHA', 'Chile':'CHI', 'China':'CHN', 'Colombia':'COL', 'Comoros':'COM', 'Congo':'CGO', 'Congo Dem Rep':'COD', 'Cook Islands':'COK', 'Costa Rica':'CRC', "Cote d'Ivoire":'CIV', 'Croatia':'CRO', 'Cuba':'CUB', 'Cyprus':'CYP', 'Czech Republic':'CZE', 'Denmark':'DEN', 'Djibouti':'DJI', 'Dominica':'DMA', 'Dominican Republic':'DOM', 'East Timor (Timor-Leste)':'TLS', 'Ecuador':'ECU', 'Egypt':'EGY', 'El Salvador':'ESA', 'Equatorial Guinea':'GEQ', 'Eritrea':'ERI', 'Estonia':'EST', 'Ethiopia':'ETH', 'Fiji':'FIJ', 'Finland':'FIN', 'France':'FRA', 'Gabon':'GAB', 'Gambia':'GAM', 'Georgia':'GEO', 'Germany':'GER', 'Ghana':'GHA', 'Greece':'GRE', 'Grenada':'GRN', 'Guam':'GUM', 'Guatemala':'GUA', 'Guinea':'GUI', 'Guinea-Bissau':'GBS', 'Guyana':'GUY', 'Haiti':'HAI', 'Honduras':'HON', 'Hong Kong':'HKG', 'Hungary':'HUN', 'Iceland':'ISL', 'India':'IND', 'Indonesia':'INA', 'Iran':'IRI', 'Iraq':'IRQ', 'Ireland':'IRL', 'Israel':'ISR', 'Italy':'ITA', 'Jamaica':'JAM', 'Japan':'JPN', 'Jordan':'JOR', 'Kazakhstan':'KAZ', 'Kenya':'KEN', 'North Korea':'PRK', 'South Korea':'KOR', 'Kuwait':'KUW', 'Kyrgyzstan':'KGZ', 'Laos':'LAO', 'Latvia':'LAT', 'Lebanon':'LIB', 'Lesotho':'LES', 'Liberia':'LBR', 'Libya':'LBA', 'Liechtenstein':'LIE', 'Lithuania':'LTU', 'Luxembourg':'LUX', 'Macedonia':'MKD', 'Madagascar':'MAD', 'Malawi':'MAW', 'Malaysia':'MAS', 'Maldives':'MDV', 'Mali':'MLI', 'Malta':'MLT', 'Mauritania':'MTN', 'Mauritius':'MRI', 'Mexico':'MEX', 'Micronesia':'FSM', 'Moldova':'MDA', 'Monaco':'MON', 'Mongolia':'MGL', 'Morocco':'MAR', 'Mozambique':'MOZ', 'Burma':'MYA', 'Namibia':'NAM', 'Nauru':'NRU', 'Nepal':'NEP', 'Netherlands':'NED', 'Netherlands Antilles':'AHO', 'New Zealand':'NZL', 'Nicaragua':'NCA', 'Niger':'NIG', 'Nigeria':'NGR', 'Norway':'NOR', 'Oman':'OMA', 'Pakistan':'PAK', 'Palau':'PLW', 'Palestine':'PLE', 'Panama':'PAN', 'Papua New Guinea':'PNG', 'Paraguay':'PAR', 'Peru':'PER', 'Philippines':'PHI', 'Poland':'POL', 'Portugal':'POR', 'Puerto Rico':'PUR', 'Qatar':'QAT', 'Romania':'ROM', 'Russia':'RUS', 'Rwanda':'RWA', 'Saint Kitts and Nevis':'SKN', 'Saint Lucia':'LCA', 'Saint Vincent and the Grenadines':'VIN', 'Samoa':'SAM', 'San Marino':'SMR', 'Sao Tome and Principe':'STP', 'Saudi Arabia':'KSA', 'Senegal':'SEN', 'Serbia':'SCG', 'Seychelles':'SEY', 'Sierra Leone':'SLE', 'Singapore':'SIN', 'Slovakia':'SVK', 'Slovenia':'SLO', 'Solomon Islands':'SOL', 'Somalia':'SOM', 'South Africa':'RSA', 'Spain':'ESP', 'Sri Lanka':'SRI', 'Sudan':'SUD', 'Suriname':'SUR', 'Swaziland':'SWZ', 'Sweden':'SWE', 'Switzerland':'SUI', 'Syria':'SYR', 'Chinese Taipei':'TPE', 'Tajikistan':'TJK', 'Tanzania':'TAN', 'Thailand':'THA', 'Togo':'TOG', 'Tonga':'TGA', 'Trinidad and Tobago':'TRI', 'Tunisia':'TUN', 'Turkey':'TUR', 'Turkmenistan':'TKM', 'Uganda':'UGA', 'Ukraine':'UKR', 'United Arab Emirates':'UAE', 'Great Britain':'GBR', 'United States':'USA', 'Uruguay':'URU', 'Uzbekistan':'UZB', 'Vanuatu':'VAN', 'Venezuela':'VEN', 'Vietnam':'VIE', 'Virgin Islands':'ISV', 'Yemen':'YEM', 'Zambia':'ZAM', 'Zimbabwe':'ZIM'}
	disciplineToSport = {'Swimming':'Aquatics', 'Athletics':'Athletics', 'Cycling':'Cycling', 'Fencing':'Fencing', 'Gymnastics':'Gymnastics', 'Water':'Aquatics', 'Cricket':'Cricket', 'Croquet':'Croquet', 'Equestrian':'Equestrian', 'Fencing':'Fencing', 'Football':'Football', 'Golf':'Golf', 'Polo':'Polo', 'Rowing':'Rowing', 'Rugby':'Rugby', 'Sailing':'Sailing', 'Shooting':'Shooting', 'Tennis':'Tennis', 'Wrestling':'Wrestling', 'Weightlifting':'Weightlifting', 'Volleyball':'Volleyball', 'Beach volley':'Volleyball', 'Triathlon':'Triathlon', 'Taekwondo':'Taekwondo', 'Table Tennis':'Table Tennis', 'Softball':'Softball', 'Judo':'Judo', 'Hockey':'Hockey', 'Handball':'Handball', 'Diving':'Diving', 'Badminton':'Badminton', 'Basketball':'Basketball', 'Canoeing':'Canoe / Kayak', 'Baseball':'Baseball', 'Boxing':'Boxing', 'Field':'Field', 'Table':'Table Tennis', 'Archery':'Archery', 'Synchronized':'Aquatics', 'Beach':'Volleyball', 'Modern':'Modern Pentathlon'}

	file = open('2016.txt')
	rows = []
	row = []
	for line in file:
		if '2016 Summer Olympics ~ ' in line:
			row.append('2016')
			split = line.split(' ')
			i = split.index('title="')
			discipline = split[i+1]
			row.append(disciplineToSport.get(discipline))
			row.append(discipline)
			j = split.index('~')
			k = split.index('">details</a></span></th>\r\n')
			event = split[j+1:k]
			if "Men's" in event:
				event.remove("Men's")
			if "Women's" in event:
				event.remove("Women's")
			row.append(" ".join(event))
			gender = ''
			if "Men" in line:
				gender = 'M'
			elif "Women" in line:
				gender = 'F'
			else:
				gender = 'X'
			row.append(gender)

			goldLine = file.next()
			if "TEAM" not in goldLine:
				row.append("Gold")
				row.append(goldLine.strip())
				goldCountry = file.next()
				row.append(countryDict.get(goldCountry.split("; ")[1].strip()))
				rows.append(row)
				row = row[:]

				row[5] = "Silver"
				next = file.next()
				row[6] = next.strip()
				silverCountry = file.next()
				row[7] = countryDict.get(silverCountry.split("; ")[1].strip())
				rows.append(row)
				row = row[:]

				row[5] = "Bronze"
				row[6] = file.next().strip()
				bronzeCountry = file.next()
				row[7] = countryDict.get(bronzeCountry.split("; ")[1].strip())
				rows.append(row)
				row = row[:]
			else:
				countryLine = file.next().strip()
				country =countryDict.get(countryLine.split("; ")[1])
				name = file.next()
				row.append("medal")
				row.append("name")
				row.append("country")

				while ("#160" not in name):
					row[5] = "Gold"
					row[6] = name.strip()
					row[7] = country
					rows.append(row)
					row = row[:]
					name = file.next()

				country = countryDict.get(name.split("; ")[1].strip())
				name = file.next()
				while ("#160" not in name):
					row[5] = "Silver"
					row[6] = name.strip()
					row[7] = country
					rows.append(row)
					row = row[:]
					name = file.next()

				country = countryDict.get(name.split("; ")[1].strip())
				name = file.next()
				while ("</tr>" not in name):
					row[5] = "Bronze"
					row[6] = name.strip()
					row[7] = country
					rows.append(row)
					row = row[:]
					name = file.next()	
		row = []

	with open('transformedTables2016.csv', 'wb') as csvfile:
		writer = csv.writer(csvfile, delimiter=',')
		for r in rows:
			writer.writerow(r)




#year, sport, discipline, event, gender, medal, name, ioc_code